# Implementar a la base de dades edt.org les següents ACLS:

 1. L’usuari “Anna Pou” és ajudant de l’administrador i té permisos per modificar-ho tot. Tothom pot veure totes les dades de tothom.



```
acces to * 
	by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
	by * read
```

2.  L’usuari “Anna Pou” és ajudant d’administració. Tothom es pot modificar el seu propi email i homePhone. Tothom pot veure totes les dades de tothom.

```
acces to attrs=mail,homephone
    by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
	by self write
	by * read

acces to * 
	by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
	by * read

```

3. Tot usuari es pot modificar el seu mail. Tothom pot veure totes les dades de tothom.
```
acces to attrs=mail
	by self write
	by * read
acces to *
	by * read
```
4. Tothom pot veure totes les dades de tothom, excepte els mail dels altres.
```
acces to attrs=mail
	by self read
acces to *
	by * read
```
5. Tot usuari es pot modificar el seu propi password i tothom pot veure totes les dades de tothom.
```
acces to attrs=userpassword
	by self write
	by * auth
	by * read
acces to *
	by * read	
```
6. Tot usuari es pot modificar el seu propi password i tothom pot veure totes les dades de tothom, excepte els altres passwords.
```
acces to attrs=userpassword
	by self write
	by * auth
	by * none
acces to *
	by * read	
```
7. Tot usuari es pot modificar el seu propi password i tot usuari només pot veure les seves pròpies dades.
```
acces to attrs=userpassword
	by self write
	by * auth
	by * none
acces to *
	by self read
```
8. Tot usuari pot observar les seves pròpies dades i modificar el seu propi password, email i homephone. L’usuari “Anna Pou” pot modificar tots els atributs de tots excepte els passwords, que tampoc pot veure. L’usuari “Pere Pou” pot modificar els passwords de tothom.
```
acces to attrs=userPassword
	by dn.exact=”cn=Pere Pou,ou=usuaris,dc=edt,dc=org" write
	by self write
	by * auth
acces to attrs=mail,homephone
	by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
	by self write
acces to *
	by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
	by self read
    by * search
```

9. Els usuaris es poden modificar el passwd, el mail i el homephone. No poden veure el passwd dels altres usuaris ni el homephone. La resta d'atributs tothom els pot veure tots.

```
acces to attrs=userpassword,mail,homephone
    by self write
    by * auth
acces to attrs=mail
    by * self write
    by * read
acces to attrs=homephone
    by * self write
acces to *
    by * read


```

10. Anna Pou fa d'administradora i ho pot modificar tot de tothom. Pere Pou fa d'ajudant i pot modificar el mail i el homephone de tothom. Tothom pot veure les dades de tothom axcepte el passwd.

```
access to attrs=userpassword
    by  dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
    by * auth
access to attrs=mail,homephone
    by dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
    by dn.exact=”cn=Pere Pou,ou=usuaris,dc=edt,dc=org" write 
    by * read
acces to *
     dn.exact=”cn=Anna Pou,ou=usuaris,dc=edt,dc=org" write 
    by * read


```
