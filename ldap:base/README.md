# LDAP 2022
## @mporto M06-ASIX
### Ldapserver 2022

```
Configuració bàsica d''un servidor LDAP en un debian

```

# PASOS A SEGUIR:

1)Creacio de docker -> docker run --name ldap.edt.org -h ldap.edt.org -it debian
Instal·lació d'alguns utils necessaris -> apt-get install procps iproute2 tree nmap vim slapd ldap-utils

2)Esborrar directoris amb contingut per defecte
/etc/ldap/slapd.d	/var/lib/ldap

3)Creació de fitxer de configuració: slapd.conf
Generar configuracio dinamica -> slaptest -f slapd.conf -F /etc/ldap/slapd.d

4)Carrega de dades inicial massiva (populate) a baix nivell(dimoni parat, accedint a fitxers de BD):
	- slapadd -F /etc/ldap/slapd.d -l edt.ldif

5)Canvi de propietari:
	- chown -R openldap.openldap /etc/ldap/slapd.d /var/lib/ldap

6) Engeguem dimoni -> /usr/sbin/slapd

7) Verifiquem que funciona amb: ps ax | nmap localhost | ldapsearch -x -LLL -b 'dc=edt,dc=org'

**Dades Clau:**

```

docker run --rm --name ldap.edt.org -h 2hisx -p 389:389 -d mporto20/ldap22:base 

PORT LDAP = 389

<<<<<<< HEAD
PORT LDAP = 389

=======
>>>>>>> 8734c3933c4726172b295c61ade4be91348e727e
dn = domain
Entitats -> dc=edt,dc=org (organization)
	- usuaris -> ou(organizationalunit)
		-Pau Pou -> cn=pau pou(rdn; relative distinguished name)

```
