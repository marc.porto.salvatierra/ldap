### Crear una nova imatge ldap: mporto20/ldap22:groups
- Fet també que sigui la imatge latest mporto20/ldap22:latest
- modificar el fitxer edt.org.ldif per afegir una ou grups.
- definir els següents grups:
	- alumnes(600), professors(601), 1asix(610), 2asix(611), wheel(10), 1wiam(612), 2wiam(613), 1hiaw(614).

- Els grups han de ser elements posixGroup
- verificar el llistat dels usuaris i grups i la coherència de dades entre els usuaris que ja teníem i els nous grups creats.
- Modificar el startup.sh perquè el servei ldap escolti per tots els protocols: ldap ldaps i ldapi.

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22:groups 

docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d mporto20/ldap22:phpldapadmin
```
