# LDAP 2022
## @mporto M06-ASIX
### Ldapserver 2022


* **mporto20/ldap22:schema** 

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22:schema


docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d mporto20/ldap22:phpldapadmin


```

