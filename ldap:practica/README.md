#LDAPSERVER 2022
## @Marcporto

###Crear una nova imatge docker anomenada: ldap21:practica
- Prjar-la al git
- Pujar-la al dockerhub
- Generar els README.md apropiats
- Crear un schema amb:
	- Un nou objecte STRUCTURAL
	- Un nou objecte AUXILIARU
	- Cada objecte ha de tenir almenys 3 nous atributs.
	- Heu d’utilitzar almenys els atributes de tipus boolean, foto (imatge jpeg) i
binary per contenir documents pdf.
- Crear una nova ou anomenada practica.
- Crear almenys 3 entitats noves dins de ou=practica que siguin dels objectClass
definits en l’schema.
	- Assegurar-se de omplir amb dades reals la foto i el pdf.
- Eliminar del slapd.conf tots els schema que no facin falta, deixar només els
imprescindibles
- Visualitzeu amb phpldapadmin les dades, observeu l’schema i verifiqueu la foto i el
pdf.

**Ordre per iniciar la imatge de ldap+phpldapadmin**
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22:practica

docker run --rm --name phpldapadmin.edt.org -h phpldapadmin.edt.org --net 2hisx -p 80:80 -d mporto20/ldap22:phpldapadmin
```
