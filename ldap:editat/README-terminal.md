# LDAP 2022
## @mporto M06-ASIX
### Ldapserver 2022

```
Ordres necessàries per construir la nostra imatge de LDAP

```

Configurar el password de Manager que sigui ‘secret’ però encriptat 
{SSHA}wTI5jgdBh2N9Qs2C9XJ4VIUTkT1kXUW0 -> slappasswd

Configurar la base de dades cn=config amb un usuari administrador anomenat syadmin i password syskey
{SSHA}xWSSeNMDhPFwL8t5FB2Wa1ChhEWqR3If -> slappasswd

docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d mporto20/ldap22:editat

