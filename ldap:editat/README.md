# LDAP 2022
## @mporto M06-ASIX
### Ldapserver 2022

```
Configuració bàsica d'un servidor LDAP editat en un debian

```

**mporto20/ldap22:editat**

# Passos a seguir:

- Generar un sol fitxer ldif anomenat edt-org.ldif (usuaris01-10)
- Afegir en el fitxer dos usuaris i una ou nova inventada i posar-los dins la nova ou.
- Modificar el fitxer edt.org.ldif modificant dn dels usuaris utilitzant en lloc del cn el uid per identificar-los (rdn).
- Configurar el password de Manager que sigui ‘secret’ però encriptat (posar-hi un comentari per indicar quin és de cara a estudiar).
- Propagar el port amb -p -P
- Editar els dos README, en el general afegir que tenim una nova imatge. En el de la imatge ldap22:editat
- Descriure els canvis i les ordres per posar-lo en marxa.
- Configurar la base de dades cn=config amb un usuari administrador anomenat syadmin i password syskey.
- Verifiqueu que sou capaços de modificar la configuració de la base de dades edt.org en calent modificant per exemple el passwd o el nom o els permisos.
